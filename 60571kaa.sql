-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Мар 18 2021 г., 15:21
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571kaa(1)`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auto`
--

CREATE TABLE `auto` (
  `auto_ID` int NOT NULL,
  `user_id` int NOT NULL,
  `brand` text NOT NULL,
  `model` text NOT NULL,
  `year` int NOT NULL,
  `fuel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `auto`
--

INSERT INTO `auto` (`auto_ID`, `user_id`, `brand`, `model`, `year`, `fuel`) VALUES
(1, 1, 'bmw', 'x1', 2000, 'Бензин'),
(2, 2, 'лада', '2110', 2004, 'Бензин'),
(3, 3, 'лада', 'приора', 2010, 'бензин'),
(4, 4, 'audi', 'a4', 2010, 'Бензин'),
(5, 5, 'лада ', '2109', 2005, 'бензин'),
(6, 1, 'bmw', 'x7', 2020, 'Дизель'),
(7, 3, 'лада', 'ларгус', 2016, 'Бензин');

-- --------------------------------------------------------

--
-- Структура таблицы `carwash`
--

CREATE TABLE `carwash` (
  `carwash_ID` int NOT NULL,
  `auto_ID` int NOT NULL,
  `typeofwash` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Тип мойки',
  `carwashcost` int NOT NULL COMMENT 'Стоимость мойки',
  `mileage` int NOT NULL COMMENT 'Пробег',
  `date` date NOT NULL COMMENT 'Дата'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `carwash`
--

INSERT INTO `carwash` (`carwash_ID`, `auto_ID`, `typeofwash`, `carwashcost`, `mileage`, `date`) VALUES
(1, 4, 'кузов с пеной', 500, 10000, '2021-03-01'),
(2, 2, 'экспресс', 200, 56000, '2021-03-03'),
(3, 5, 'кузов коврики', 600, 56000, '2021-03-10'),
(4, 6, 'комплекс', 1200, 5000, '2021-03-01'),
(5, 7, 'комплекс', 1000, 70000, '2021-03-01');

-- --------------------------------------------------------

--
-- Структура таблицы `Cheque`
--

CREATE TABLE `Cheque` (
  `cheque_ID` int NOT NULL,
  `auto_ID` int NOT NULL,
  `fuel_ID` int NOT NULL,
  `litres` int NOT NULL COMMENT 'Кол-во литров',
  `cost` int NOT NULL COMMENT 'Стоимость',
  `sum` int NOT NULL COMMENT 'Сумма',
  `mileage` int NOT NULL COMMENT 'Пробег',
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Cheque`
--

INSERT INTO `Cheque` (`cheque_ID`, `auto_ID`, `fuel_ID`, `litres`, `cost`, `sum`, `mileage`, `data`) VALUES
(1, 4, 3, 10, 50, 500, 10000, '2021-03-01'),
(2, 2, 1, 20, 40, 800, 56000, '2021-03-03'),
(3, 7, 1, 15, 40, 600, 70000, '2021-03-02'),
(4, 4, 3, 40, 50, 2000, 10500, '2021-03-03'),
(5, 5, 2, 10, 45, 450, 56000, '2021-03-03'),
(6, 6, 4, 25, 53, 1325, 5000, '2021-03-05');

-- --------------------------------------------------------

--
-- Структура таблицы `diagcard`
--

CREATE TABLE `diagcard` (
  `diagcard_ID` int NOT NULL,
  `auto_ID` int NOT NULL,
  `cost` int NOT NULL COMMENT 'Стоимость',
  `date` date NOT NULL COMMENT 'Дата',
  `mileage` int NOT NULL COMMENT 'Пробег'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `diagcard`
--

INSERT INTO `diagcard` (`diagcard_ID`, `auto_ID`, `cost`, `date`, `mileage`) VALUES
(1, 1, 1000, '2021-03-17', 10000),
(2, 2, 650, '2021-03-31', 60000),
(3, 3, 650, '2021-03-24', 30000),
(4, 4, 800, '2021-03-23', 10500),
(5, 5, 450, '2021-03-23', 57000),
(6, 6, 1230, '2021-03-18', 6000),
(7, 7, 670, '2021-03-19', 70500);

-- --------------------------------------------------------

--
-- Структура таблицы `fuel`
--

CREATE TABLE `fuel` (
  `fuel_ID` int NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'Тип топлива'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `fuel`
--

INSERT INTO `fuel` (`fuel_ID`, `type`) VALUES
(1, '92'),
(2, '95'),
(3, '100'),
(4, 'дт');

-- --------------------------------------------------------

--
-- Структура таблицы `insurance`
--

CREATE TABLE `insurance` (
  `insurance_ID` int NOT NULL,
  `auto_ID` int NOT NULL,
  `typeofinsur` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Тип Страхования',
  `mileage` int NOT NULL COMMENT 'Пробег',
  `date` date NOT NULL COMMENT 'Дата',
  `cost_insur` int NOT NULL COMMENT 'Стоимость'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `insurance`
--

INSERT INTO `insurance` (`insurance_ID`, `auto_ID`, `typeofinsur`, `mileage`, `date`, `cost_insur`) VALUES
(1, 1, 'Каско', 4000, '2021-02-02', 25500),
(2, 2, 'Осаго', 58000, '2021-03-20', 4200),
(3, 3, 'Осаго', 30000, '2021-02-14', 5000),
(4, 4, 'Каско+', 9000, '2021-02-02', 45670),
(5, 5, 'осаго', 54000, '2021-02-09', 6780),
(6, 6, 'Каско+ремонт у дилера', 4200, '2021-02-12', 89450),
(7, 7, 'осаго 3 месяца', 68500, '2021-02-04', 1240);

-- --------------------------------------------------------

--
-- Структура таблицы `service`
--

CREATE TABLE `service` (
  `service_ID` int NOT NULL,
  `auto_ID` int NOT NULL,
  `typeofwork` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Тип работ',
  `servicename` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Название сервиса',
  `workcost` int NOT NULL COMMENT 'Стоимость работ',
  `detcost` int NOT NULL COMMENT 'Стоимость детали',
  `mileage` int NOT NULL COMMENT 'Пробег',
  `date` date NOT NULL COMMENT 'Дата'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `service`
--

INSERT INTO `service` (`service_ID`, `auto_ID`, `typeofwork`, `servicename`, `workcost`, `detcost`, `mileage`, `date`) VALUES
(1, 1, 'Замена масла', 'СТО Маяк', 560, 2400, 12000, '2021-03-31'),
(2, 2, 'замена шаровой', 'сто маяк', 350, 360, 58000, '2021-02-01'),
(3, 3, 'регулировка клапанов', 'сто автоваз', 1000, 0, 30000, '2021-02-10'),
(4, 4, 'чистка радиаторов', 'ауди сервис сургут', 16500, 1000, 8600, '2021-01-13'),
(5, 5, 'шиномонтаж', 'вираж', 670, 30, 59000, '2021-03-31'),
(6, 6, 'прошивка коробки', 'сто айсберг', 35000, 0, 5000, '2021-02-01'),
(7, 7, 'замена лампочки', 'сто маяк', 200, 340, 77000, '2021-04-14'),
(8, 3, 'замена масла', 'масленка', 600, 1500, 20000, '2020-12-02'),
(9, 6, 'развал-схождение', 'официальный дилер', 2400, 0, 8000, '2021-04-01');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `user_ID` int NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_ID`, `login`, `password`) VALUES
(1, 'User1', 123),
(2, 'User2', 123),
(3, 'User3', 123),
(4, 'User4', 123),
(5, 'User5', 123);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`auto_ID`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `carwash`
--
ALTER TABLE `carwash`
  ADD PRIMARY KEY (`carwash_ID`),
  ADD KEY `auto_ID` (`auto_ID`);

--
-- Индексы таблицы `Cheque`
--
ALTER TABLE `Cheque`
  ADD PRIMARY KEY (`cheque_ID`),
  ADD KEY `auto_ID` (`auto_ID`),
  ADD KEY `fuel_ID` (`fuel_ID`);

--
-- Индексы таблицы `diagcard`
--
ALTER TABLE `diagcard`
  ADD PRIMARY KEY (`diagcard_ID`),
  ADD KEY `auto_ID` (`auto_ID`);

--
-- Индексы таблицы `fuel`
--
ALTER TABLE `fuel`
  ADD PRIMARY KEY (`fuel_ID`);

--
-- Индексы таблицы `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`insurance_ID`),
  ADD KEY `auto_ID` (`auto_ID`);

--
-- Индексы таблицы `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_ID`),
  ADD KEY `auto_ID` (`auto_ID`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `auto`
--
ALTER TABLE `auto`
  MODIFY `auto_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `carwash`
--
ALTER TABLE `carwash`
  MODIFY `carwash_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `Cheque`
--
ALTER TABLE `Cheque`
  MODIFY `cheque_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `diagcard`
--
ALTER TABLE `diagcard`
  MODIFY `diagcard_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `fuel`
--
ALTER TABLE `fuel`
  MODIFY `fuel_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `insurance`
--
ALTER TABLE `insurance`
  MODIFY `insurance_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `service`
--
ALTER TABLE `service`
  MODIFY `service_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `user_ID` int NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auto`
--
ALTER TABLE `auto`
  ADD CONSTRAINT `auto_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `carwash`
--
ALTER TABLE `carwash`
  ADD CONSTRAINT `carwash_ibfk_1` FOREIGN KEY (`auto_ID`) REFERENCES `auto` (`auto_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Cheque`
--
ALTER TABLE `Cheque`
  ADD CONSTRAINT `Cheque_ibfk_1` FOREIGN KEY (`auto_ID`) REFERENCES `auto` (`auto_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Cheque_ibfk_2` FOREIGN KEY (`fuel_ID`) REFERENCES `fuel` (`fuel_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `diagcard`
--
ALTER TABLE `diagcard`
  ADD CONSTRAINT `diagcard_ibfk_1` FOREIGN KEY (`auto_ID`) REFERENCES `auto` (`auto_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `insurance`
--
ALTER TABLE `insurance`
  ADD CONSTRAINT `insurance_ibfk_1` FOREIGN KEY (`auto_ID`) REFERENCES `auto` (`auto_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_1` FOREIGN KEY (`auto_ID`) REFERENCES `auto` (`auto_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
