<?php namespace App\Controllers;

use App\Models\RatingModel;

class Rating extends BaseController


{
    private $view;


    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RatingModel();
        $data ['rating'] = $model->getRating();
        echo view('rating/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RatingModel();
        $data ['rating'] = $model->getRating($id);
        $this->view = view('rating/view', $this->withIon($data));
        echo $this->view;
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('rating/create', $this->withIon($data));
    }
    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
//                'id'  => 'required',
                'date' => 'required',
                'detcost'  => 'required',
                'typeofwork'  => 'required',
                'workcost'  => 'required',
                'servicename'  => 'required',
                'mileage'  => 'required',
                'auto_ID'  => 'required',
            ]))
        {
            $model = new RatingModel();
            $model->save([
                'date' => $this->request->getPost('date'),
                'detcost' => $this->request->getPost('detcost'),
                'typeofwork' => $this->request->getPost('typeofwork'),
                'workcost' => $this->request->getPost('workcost'),
                'servicename' => $this->request->getPost('servicename'),
                'mileage' => $this->request->getPost('mileage'),
                'auto_ID' => $this->request->getPost('auto_ID'),
            ]);
            session()->setFlashdata('message', lang('Curating.rating_create_success'));
            return redirect()->to('/rating');
        }
        else
        {
            return redirect()->to('/rating/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RatingModel();

        helper(['form']);
        $data ['rating'] = $model->getRating($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('rating/edit', $this->withIon($data));

    }
    public function update()
    {
        helper(['form','url']);
        echo '/rating/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'date' => 'required',
                'detcost'  => 'required',
                'typeofwork'  => 'required',
                'workcost'  => 'required',
                'servicename'  => 'required',
                'mileage'  => 'required',
                'auto_ID'  => 'required',
            ]))
        {
            $model = new RatingModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'date' => $this->request->getPost('date'),
                'detcost' => $this->request->getPost('detcost'),
                'typeofwork' => $this->request->getPost('typeofwork'),
                'workcost' => $this->request->getPost('workcost'),
                'servicename' => $this->request->getPost('servicename'),
                'mileage' => $this->request->getPost('mileage'),
                'auto_ID' => $this->request->getPost('auto_ID'),
            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/rating');
        }
        else
        {
            return redirect()->to('/rating/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RatingModel();
        $model->delete($id);
        return redirect()->to('/rating');
    }
    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            $model = new RatingModel();
            $data['rating'] = $model->getRatingWithUser()->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            $data['rating'] = $model->getRatingWithUser();
            echo view('rating/view_all_with_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }




}