<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Servicetables extends Migration
{
    public function up()
    {// Drop table 'auto' if it exists
        $this->forge->dropTable('auto', true);
        // Table structure for table 'auto'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'user_ID' => [
                'type'       => 'INT',
                'unsigned'       => true,
                'null' => false,
            ],
            'brand' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'model' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'year' => [
                'type'       => 'INT',
                'null' => false,
            ],
            'fuel' => [
                'type'       => 'INT',
                'null' => false,
            ],
        ]);
        $this->forge->addKey('id', true);

        $this->forge->createTable('auto');


        // Drop table 'service' if it exists
        $this->forge->dropTable('service', true);
        // Table structure for table 'service'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'auto_ID' => [
                'type'       => 'INT',
                'unsigned'       => true,
                'null' => false,
            ],
            'typeofwork' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'servicename' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'workcost' => [
                'type'       => 'INT',
                'null' => false,
            ],
            'detcost' => [
                'type'       => 'INT',
                'null' => false,
            ],
            'mileage' => [
                'type'       => 'INT',
                'null' => false,
            ],
            'date' => [
                'type'       => 'DATE',
                'null' => false,
            ],
        ]);
        $this->forge->addKey('id', true);

        $this->forge->addForeignKey('auto_ID', 'auto', 'id', 'RESTRICT', 'RESTRICT');

        $this->forge->createTable('service');

    }

    public function down()
    {
        $this->forge->dropTable('auto', true);
        $this->forge->dropTable('service', true);
    }
}
