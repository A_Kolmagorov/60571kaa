<?php


namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ServiceSeeds extends Seeder
{
    public function run()
    {
        $data = [
            'user_ID' => '1',
            'brand' => 'bmw',
            'model' => 'x4',
            'year' => '2015',
            'fuel' => 'Бензин',
        ];
        // Using Query Builder
        $this->db->table('auto')->insert($data);
        $data = [
            'user_ID' => '1',
            'brand' => 'bmw',
            'model' => 'x3',
            'year' => '2008',
            'fuel' => 'Бензин',
        ];
        // Using Query Builder
        $this->db->table('auto')->insert($data);
        $data = [
            'user_ID' => '1',
            'brand' => 'bmw',
            'model' => 'x7',
            'year' => '2020',
            'fuel' => 'Бензин',
        ];
        // Using Query Builder
        $this->db->table('auto')->insert($data);
        $data = [
            'auto_ID' => '1',
            'typeofwork' => 'Замена Ож',
            'servicename' => 'Маяк',
            'workcost' => '500',
            'detcost' => '500',
            'mileage' => '54000',
            'date' => '2020-05-05',
        ];
        // Using Query Builder
        $this->db->table('service')->insert($data);
        $data = [
            'auto_ID' => '1',
            'typeofwork' => 'Замена масла',
            'servicename' => 'Маяк',
            'workcost' => '500',
            'detcost' => '500',
            'mileage' => '54000',
            'date' => '2020-05-05',
        ];
        // Using Query Builder
        $this->db->table('service')->insert($data);
        $data = [
            'auto_ID' => '1',
            'typeofwork' => 'Замена клапанной крышки',
            'servicename' => 'ОД',
            'workcost' => '1000',
            'detcost' => '2500',
            'mileage' => '78000',
            'date' => '2020-12-12',
        ];
        // Using Query Builder
        $this->db->table('service')->insert($data);
        $data = [
            'auto_ID' => '1',
            'typeofwork' => 'Замена шаровой',
            'servicename' => 'Маяк-1',
            'workcost' => '400',
            'detcost' => '350',
            'mileage' => '28000',
            'date' => '2021-09-09',
        ];
        // Using Query Builder
        $this->db->table('service')->insert($data);
    }
}
