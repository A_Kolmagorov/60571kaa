<?php namespace App\Models;
use CodeIgniter\Model;
class RatingModel extends Model
{
    protected $table = 'service'; //таблица, связанная с моделью
    protected $allowedFields = ['date', 'detcost', 'typeofwork', 'workcost', 'servicename', 'mileage', 'auto_ID', 'id'];
    public function getRating($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
    public function getRatingWithUser($id = null)
    {
        $builder = $this->select('*')->join('auto','service.auto_id = auto.id');
        if (!is_null($id))
        {
            return $builder->where(['auto.id' => $id])->first();
        }
        return $builder->paginate(2, 'group1');
    }
}