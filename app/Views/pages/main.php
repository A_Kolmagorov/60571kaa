<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?= $this->section('content') ?>
<div class="container">
    <div class="row align-items-center">
        <div class="col-6">
            <h1 class="header__title">
                Car passports
            </h1>
            <p class="header__subtitle">
                Отслеживайте расходы на обслуживание личного автомобиля онлайн!
            </p>
            <div class="header__btn-group">
                <a href="auth/register_user" class="button button_outline" role="button">
                    Зарегестрироватся
                </a>

            </div>
        </div>
        <div class="col-6">
            <img src="images/header.jpg" alt="">
        </div>
    </div>
</div>
<?= $this->endSection() ?>
