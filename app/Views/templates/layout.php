<!DOCTYPE html>
<head>
    <title>Универсальная рейтинговая система CURaing</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="/styles/style.css"/>
</head>
<body>
<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href="<?= base_url() ?>"> <img src="/images/logo.jpg" alt=""></a>
        <button class="order-1 order-lg-0 navbar-toggler p-0" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"><span class="iconify" data-icon="eva:menu-fill"
                                        data-inline="false"></span></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav w-100">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true">Сервис
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?= base_url() ?>/index.php/rating">Все записи</a>
                        <a class="dropdown-item" href="<?= base_url()?>/rating/store">Создать запись</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">О нас</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>/pages/view/agreement">Пользовательское соглашение</a>
                </li>
                <?php use IonAuth\Libraries\IonAuth;
                $ionAuth = new IonAuth();
                ?>
                <?php if (! $ionAuth->loggedIn()): ?>
                <li class="nav-item ml-auto mr-0">
                    <a class="nav-link  button button_primary" href="<?= base_url() ?>/auth/login">Войти</a>
                </li>
                <?php else: ?>
                <li class="nav-item ml-auto mr-0">
                    <a class="nav-link button button_primary" href="<?= base_url() ?>/auth/logout">Выйти</a>
                </li>
                <?php endif ?>
            </ul>
        </div>
    </div>

</nav>
<?php if (session()->getFlashdata('message')) :?>
    <div class="alert alert-info" role="alert" style="max-width: 540px;">
        <?= session()->getFlashdata('message') ?>
    </div>
<?php endif ?>

<?= $this->renderSection('content') ?>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
</body>
</html>