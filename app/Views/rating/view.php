<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($rating)) : ?>
        <div class="card mb-3">
            <div class="row">
                <div class="col-3">
                    <div> typeofwork </div>
                    <?= esc($rating['typeofwork']); ?>
                </div>
                <div class="col-3">
                    <div> workcost </div>
                    <?= esc($rating['workcost']); ?>
                </div>

                <div class="col-3">
                <div> detcost </div>
                     <?= esc($rating['detcost']); ?>
                </div>


                   <div class="col-3">
                <div> mileage </div>
                     <?= esc($rating['mileage']); ?>
                </div>

                            <div class="col-3">
                    <div> servicename </div>
                    <?= esc($rating['servicename']); ?>
                </div>

                     <div class="col-3">
                    <div> service_ID </div>
                    <?= esc($rating['id']); ?>
                       </div>

                    <div class="col-3">
                           <div> auto_ID </div>
                          <?= esc($rating['auto_ID']); ?>
                         </div>
                           <div class="col-3">
                    <div> date </div>
                    <?= esc($rating['date']); ?>
                         </div>

            </div>
        </div>
    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
