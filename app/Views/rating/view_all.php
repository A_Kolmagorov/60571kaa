<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все рейтинги</h2>

        <?php if (!empty($rating) && is_array($rating)) : ?>

            <?php foreach ($rating as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['servicename']); ?></h5>
                                <p class="card-text"><?= esc($item['typeofwork']); ?></p>
                                <a href="<?= base_url() ?>/index.php/rating/view/<?= esc($item['id']); ?>"
                                   class="btn btn-primary">Просмотреть</a>
                                <a href="<?= base_url()?>/rating/edit/<?= esc($item['id']); ?>"
                                   class="btn btn-primary">Редактировать</a>
                                <a href="<?= base_url() ?>/rating/delete/<?= esc($item['id']); ?>"
                                   class="btn btn-primary">Удалить</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти рейтинги.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>