<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php if (!empty($rating) && is_array($rating)) : ?>
        <h2>Все рейтинги:</h2>
        <div class="d-flex justify-content-between mb-2">
            <?= $pager->links('group1') ?>
        </div>
            <?php foreach ($rating as $item): ?>
            <div class="card mb-3">
                <div class="row">
                    <div class="col-3">
                        <div> typeofwork </div>
                        <?= esc($item['typeofwork']); ?>
                    </div>
                    <div class="col-3">
                        <div> workcost </div>
                        <?= esc($item['workcost']); ?>
                    </div>

                    <div class="col-3">
                        <div> detcost </div>
                        <?= esc($item['detcost']); ?>
                    </div>


                    <div class="col-3">
                        <div> mileage </div>
                        <?= esc($item['mileage']); ?>
                    </div>

                    <div class="col-3">
                        <div> servicename </div>
                        <?= esc($item['servicename']); ?>
                    </div>

                    <div class="col-3">
                        <div> service_ID </div>
                        <?= esc($item['id']); ?>
                    </div>

                    <div class="col-3">
                        <div> brand </div>
                        <?= esc($item['brand']); ?>
                    </div>
                    <div class="col-3">
                        <div> model </div>
                        <?= esc($item['model']); ?>
                    </div>

                </div>
            </div>


                        <a href="<?= base_url()?>/rating/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                        <a href="<?= base_url()?>/rating/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url()?>/rating/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>


            <?php endforeach; ?>


    <?php else : ?>
        <div class="text-center">
            <p>Рейтинги не найдены </p>
            <a class="btn btn-primary btn-lg" href="<?= base_url()?>/rating/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать рейтинг</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
