<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('rating/update'); ?>
        <input type="hidden" name="id" value="<?= $rating["id"] ?>">
        <div class="form-group">
            <label for="name">Дата</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>" name="date"
                   value="<?= $rating['date']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Стоимость детали</label>
            <input type="text" class="form-control <?= ($validation->hasError('detcost')) ? 'is-invalid' : ''; ?>" name="detcost"
                   value="<?= $rating['detcost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('detcost') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="name">Тип работы</label>
            <input type="text" class="form-control <?= ($validation->hasError('typeofwork')) ? 'is-invalid' : ''; ?>" name="typeofwork"
                   value="<?= $rating['typeofwork']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('typeofwork') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Стоимость работы</label>
            <input type="text" class="form-control <?= ($validation->hasError('workcost')) ? 'is-invalid' : ''; ?>" name="workcost"
                   value="<?= $rating['workcost']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('workcost') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Название сервиса</label>
            <input type="text" class="form-control <?= ($validation->hasError('servicename')) ? 'is-invalid' : ''; ?>" name="servicename"
                   value="<?= $rating['servicename']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('servicename') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Название сервиса</label>
            <input type="text" class="form-control <?= ($validation->hasError('mileage')) ? 'is-invalid' : ''; ?>" name="mileage"
                   value="<?= $rating['mileage']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('mileage') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Название сервиса</label>
            <input type="textS" class="form-control <?= ($validation->hasError('auto_ID')) ? 'is-invalid' : ''; ?>" name="auto_ID"
                   value="<?= $rating['auto_ID']; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('auto_ID') ?>
            </div>
        </div>



        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>
<?php
